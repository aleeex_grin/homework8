package ru.inordic.ivasileva.ioStream;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import static java.nio.charset.StandardCharsets.UTF_8;

public class IOStreamImpl implements IOStream {
    private final String filePath;

    public IOStreamImpl(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void execute() throws Exception {
        Scanner scanner = new Scanner(System.in);
        File file = new File(filePath);
        boolean isExit;

        do {
            if (!file.exists()) {
                System.out.println("This file wasn't found. Do you want to create it?" + "\r\n" + "Yes or No?");

                String yesOrNo = scanner.nextLine();

                if (yesOrNo.equalsIgnoreCase("yes")) {
                    createFile();
                }
            }
            System.out.println("Select an action to work with the file:" + "\r\n" +
                    "1 - Add text to file" + "\r\n" +
                    "2 - Read file content" + "\r\n" +
                    "3 - Exit");
            int numberAction = scanner.nextInt();
            isExit = numberAction != 3;

            switch (numberAction) {
                case 1:
                    System.out.println("Enter the text you want to write");
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in, UTF_8));
                    String line = bufferedReader.readLine();
                    writeFile(line);
                    break;

                case 2:
                    readFile();
                    break;

                case 3:
                    isExit = false;
                    break;
            }
        } while (isExit);
    }

    @Override
    public void createFile() throws Exception {
        Path path = Paths.get(filePath);
        Files.createFile(path);
        System.out.println("Your file has been successfully created");
    }

    @Override
    public void writeFile(String inputText) throws Exception {
        File file = new File(filePath);
        try (OutputStream os = new FileOutputStream(file, true)) {
            os.write(inputText.getBytes(), 0, inputText.getBytes().length);
        }
    }

    @Override
    public String readFile() throws Exception {
        String result;
        Path path = Paths.get(filePath);

        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            result = bufferedReader.readLine();
            System.out.println(result != null ? "Your text is:" + "\r\n" + result + "\r\n" : "Sorry, your file is empty");
        }
        return result;
    }

}
