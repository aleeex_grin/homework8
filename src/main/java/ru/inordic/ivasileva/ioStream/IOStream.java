package ru.inordic.ivasileva.ioStream;

public interface IOStream {

    void execute() throws Exception;

    void createFile() throws Exception;

    void writeFile(String inputText) throws Exception;

    String readFile() throws Exception;

}
