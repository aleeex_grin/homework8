package ru.inordic.ivasileva.ioStream;

import java.util.Scanner;

public class RunApp {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the path to search for the file");
        String inputPath = scanner.nextLine();
        IOStreamImpl workWithFiles = new IOStreamImpl(inputPath);
        workWithFiles.execute();
        scanner.close();
    }
}
