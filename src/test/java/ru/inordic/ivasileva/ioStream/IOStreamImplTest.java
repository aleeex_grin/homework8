package ru.inordic.ivasileva.ioStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class IOStreamImplTest {

    @Test
    void readFile() throws Exception {
        String fileName = "D:/ivasilyeva.txt";
        IOStreamImpl ioStream = new IOStreamImpl(fileName);
        String readFile = ioStream.readFile();
        String result = "aaaaaaaa";

        Assertions.assertEquals(result, readFile);
        Assertions.assertNotEquals("dccn", readFile);
    }

}